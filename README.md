# Python Luigi Lab

This is an experimental project to develop some reactive programs using the Luigi library.

By the way, I suggest to use [VirtualEnv](https://virtualenv.pypa.io/) for running it without changing your system configuration.
The file `requirements.txt` describes all projects' dependencies.

http://bytepawn.com/luigi.html

